<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello(){
        // return view('hello')->with('name', 'Homer Smpson');
        // return view('hello', ['name' => 'Homer SIMPSON']);
    
        $info = array(
            'frontend' => 'Zuitt Coding Bootcamp',
            'topics' => ['HTML and CSS', 'JS DOM', 'React']
        );
        return view('hello')->with($info);  
    }

    public function about(){
        return view('pages/about');
    }

    public function services(){
        return view('pages/services');
    }

}
